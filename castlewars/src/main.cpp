///@author Kaupper

#include <SDL.h>
#include "Game.h"
#include "Player.h"
#include <iostream>
#include <sstream>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#include "MainMenu.h"
#include "Options.h"
#include "Credits.h"
#include "StartGame.h"
#include <windows.h>


KeyboardLayout(* layouts)[2];
int* resolution;
bool paused;

int main(int argc, char* argv[])
{
    if(load() == -1)
        cout<<"Failed to load config file!"<<endl;
    else
    {
        layouts = olayouts;
        mlayouts = layouts;
        resolution = &res;

        Game* game = new Game(possibleResolutions[*resolution].width, possibleResolutions[*resolution].height, "Castle Wars");
        game->addScene(loadMainMenu(game));
        game->addScene(loadOptions(game));
        game->addScene(loadCredits(game));
        game->addScene(loadGame(game));
        game->run();
        game->clear();
        delete game;
    }
    exit(0);
    return 0;
}
//    Core::getInstance(1600,900,"Ritter");
//    int w = 122;
//    int h = 180;
//    SDL_Rect* r = new SDL_Rect{0,0,w,h};
//    Animation* a = new Animation("ritter", 2);
//    stringstream filename;
//    filename.str("");
//    for(int i = 0; i <= 7; i++)
//    {
//        filename.str("");
//        filename << "ritter_comic_anime1" << i << ".png";
//        a->addImage(filename.str(), r);
//    }
//
//    Texture* text = new Texture();
//    Animation* textAnimation = new Animation("text", 0);
//    Element* textElement = new Element(new Location{400,200});
//
//    text->loadText("Castle Wars", new SDL_Color{0,0,0});
//    textAnimation->addTexture(text);
//    textElement->addAnimation(textAnimation);
//
//    Texture* rocketTex = new Texture();
//    Animation* rocketAnim = new Animation("rocket", 0);
//    rocketAnim->addImage("rocket.png", NULL );
//
//    Entity* ent = new Entity(new Location{0,500});
//    ent->addAnimation(a);
//
//    Scene* scene = new Scene(0, "ritter", NULL);
//    scene->addElement(ent);
//    scene->addElement(textElement);
//    SDL_Event event;
//    int direction = 0;
//    bool quit = false;
//    bool jump = false;
//    int jumpStartedTicks = -1;
//    Timer t;
//    Location* jumpStartLocation;
//     Entity* rocketEnt = NULL;
//    while(!quit)
//    {
//        a->setDelta(150000);
//        if(direction != 0)
//        {
//            a->setDelta(1);
//            ent->move(15 * direction, 0);
//        }
//        if(rocketEnt != NULL)
//        {
//
//            rocketEnt->loop(rocketEnt);
//        }
//        while(SDL_PollEvent(&event) != 0)
//        {
//            if(event.type == SDL_QUIT)
//                quit = true;
//            if(event.type == SDL_KEYDOWN)
//            {
//                if(event.key.keysym.sym == scene->layouts[1].right)
//                {
//                    direction = 1;
//                    ent->flipped = false;
//                }
//                else if(event.key.keysym.sym == scene->layouts[1].left)
//                {
//                    direction = -1;
//                    ent->flipped = true;
//                }
//                else if(event.key.keysym.sym == SDLK_ESCAPE)
//                {
//                    quit = true;
//                }
//                else if(event.key.keysym.sym == scene->layouts[1].up && !jump)
//                {
//                    jump = true;
//                    t.start();
//                    jumpStartedTicks = t.getTicks();
//                    jumpStartLocation = ent->getLocation();
//                }
//                else if(event.key.keysym.sym == scene->layouts[1].action)
//                {
//                    rocketEnt = new Entity(new Location{ent->location->x, ent->location->y});
//                    scene->addElement(rocketEnt);
//                    rocketEnt->addAnimation(rocketAnim);
//                    rocketEnt->loop = &rocketShootOnLoop;
//                }
//            }
//            else
//            {
//                if((event.key.keysym.sym == scene->layouts[1].right && direction == 1) || (event.key.keysym.sym == scene->layouts[1].left && direction == -1))
//                {
//                    direction = 0;
//                }
//            }
//        }
//
//            if(jump)
//            {
//                // h = t^2*g/2
//                //int h = ((pow((t.getTicks() - jumpStartedTicks) / 100, 2)) * 10) / 2;
//                int ti = (t.getTicks() - jumpStartedTicks);
//                int h = 50 * (ti / 70) - 5 * pow(ti / 70, 2) + 1;
//                cout << h << endl;
//                ent->moveTo(new Location{ent->getLocation()->x, -h + jumpStartLocation->y});
//                if(ent->getLocation()->y >= jumpStartLocation->y)
//                {
//                    jump = false;
//                    ent->moveTo(new Location{ent->getLocation()->x, jumpStartLocation->y});
//                }
//            }
//        SDL_RenderClear(Core::getInstance()->getRenderer());
//        scene->render();
//        SDL_RenderPresent(Core::getInstance()->getRenderer());
//        SDL_Delay(50);
//    }
