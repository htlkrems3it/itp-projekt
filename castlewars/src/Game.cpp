///@author Kaupper


#include "Game.h"
#include <iostream>

//FPS = 60
const float SCREEN_TICKS_PER_FRAME = 1000/60;

int Game::setActiveScene(string name)
{
    for(list<Scene*>::iterator i = scenes->begin(); i != scenes->end(); i++)
    {
        if(((Scene*)*(i))->name == name)
        {
            Game::activeScene = ((Scene*)*(i));
            return 0;
        }
    }
    return 1;
}

int Game::setActiveScene(int index)
{
    if(index < scenes->size())
    {
        list<Scene*>::iterator i = scenes->begin();
        advance(i, index);
        activeScene = *i;
        return 0;
    }
    return 1;
}

int Game::addScene(Scene* scene)
{
    scenes->push_back(scene);

    if(scenes->size() == 1)
        setActiveScene(scene->name);
    return 0;
}

int Game::loadMedia()
{

    return 0;
}

int Game::onLoop()
{

   return activeScene->loop(activeScene);
}

int Game::onEvent(SDL_Event* event)
{
    return activeScene->event(activeScene, event);
}

int Game::onRender()
{
    SDL_RenderClear(Core::getInstance()->getRenderer());
    activeScene->render();
    SDL_RenderPresent(Core::getInstance()->getRenderer());

    return 0;
}

Game::Game(int width, int height, string title)
{
    Game::core = Core::getInstance(width, height, title);
    Game::scenes = new list<Scene*>();
    running = false;
}

Game::~Game()
{
    cout << "Exiting Game" << endl;
    delete timer;
    delete core;
}

void Game::clear()
{
    for(list<Scene*>::iterator i = scenes->begin(); i != scenes->end(); i++)
    {
        (*i)->clear();
        delete (*i);
    }
    Core::getInstance()->clean();
}

int Game::run()
{
    SDL_Event event;
    Timer timer;
    running = true;

    while(running)
    {
        timer.start();
        while(SDL_PollEvent(&event) != 0)
        {
            if(event.type == SDL_QUIT)
            {
                cout << "quitting" << endl;
                running = false;
            }
            onEvent(&event);
        }
        onLoop();
        onRender();

        int frameTicks = timer.getTicks();

        if(frameTicks < SCREEN_TICKS_PER_FRAME)
            SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
        timer.stop();
    }

    return 0;
}
