///@author Kaupper

#include "Texture.h"
#include <iostream>
#include <sstream>

using namespace std;

static int texturecount = 0;
Texture::Texture()
{
    flip = false;
}

Texture::~Texture()
{
    cout << "Exiting Texture" << endl;
    Texture::clear();
}

//gibt resourcen frei
void Texture::clear()
{
    delete clip;
    SDL_DestroyTexture(texture);
    delete texture;
}

//l�dt einen text mit der angegebenen farbe und font
int Texture::loadText(string text, SDL_Color* color, short size)
{
    if(!Core::isInstanced())
        return -1;

    TTF_Font* font = NULL;
    if(size <0) font = Core::getInstance()->smallFont;
    if(size == 0)
        font = Core::getInstance()->mediumFont;
    if(size>0)
        font = Core::getInstance()->largeFont;
    SDL_Surface* textSurface = TTF_RenderText_Solid(font, text.c_str(), *color);

    if(textSurface == NULL)
        return -1;

    SDL_Texture* nTexture = NULL;

    nTexture = SDL_CreateTextureFromSurface((Core::getInstance())->getRenderer(), textSurface);

    if(nTexture == NULL)
        return -2;

    SDL_FreeSurface(textSurface);

    texture = nTexture;
    int w, h;
    SDL_QueryTexture(texture, NULL, NULL, &w, &h);
    Texture::clip = Texture::size = new SDL_Rect{0, 0, w, h};

    return 0;
}

//l�dt ein bild aus der angegebenen grafik und dem angegebenen rechteck
int Texture::loadImage(string path, SDL_Rect* clip, SDL_Rect* size)
{
    Texture::clip = clip;
    Texture::size = size;

    SDL_Texture* nTexture = NULL;
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());

    if(loadedSurface == NULL)
        return -1;

    nTexture = SDL_CreateTextureFromSurface((Core::getInstance())->getRenderer(), loadedSurface);

    if(nTexture == NULL)
        return -2;

    if(clip == NULL)
    {
        Texture::clip = new SDL_Rect{0,0,loadedSurface->w, loadedSurface->h};
    }
    if(size == NULL)
    {
        Texture::size = Texture::clip;
    }

    SDL_FreeSurface(loadedSurface);
    texture = nTexture;

    return 0;
}

//gibt die weite zur�ck
int Texture::getWidth()
{
    return size->w;
}

//gibt die h�he zur�ck
int Texture::getHeight()
{
    return size->h;
}


//rendert die texture
int Texture::render(Location* loc, bool flip)
{
    SDL_Renderer* renderer = Core::getInstance()->getRenderer();
    SDL_Rect* rect = new SDL_Rect{loc->x, loc->y, size->w, size->h};
    SDL_Point* point = new SDL_Point{getWidth() / 2, 0};

    int ret;
    if(flip)
        ret = SDL_RenderCopyEx(renderer, texture, clip, rect, 0, point, SDL_FLIP_HORIZONTAL);
    else
        ret =  SDL_RenderCopy(renderer, texture, clip, rect);

        delete rect;
        delete point;
    return ret;
}
