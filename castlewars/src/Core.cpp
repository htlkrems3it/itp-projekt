///@author Kaupper

#include "Core.h"
#include <iostream>

//singleton
Core* Core::instance = NULL;

Core::Core()
{
    //standard zuweisungen
    Core::WINDOW_TITLE  =   "Game";
    Core::WINDOW_WIDTH  =   800;
    Core::WINDOW_HEIGHT =   600;

    int rs = Core::init();
    if(rs != 0)
    {
        cout << "Core init Error " << rs;
    }
}

Core::Core(int width, int height, string windowTitle)
{
    Core::WINDOW_TITLE  =   windowTitle;
    Core::WINDOW_WIDTH  =   width;
    Core::WINDOW_HEIGHT =   height;

    int rs = Core::init();
    if(rs != 0)
    {
        cout << "Core init Error " << rs;
    }
}

SDL_Renderer* Core::getRenderer()
{
    //gibt den globalen renderer zur�ck (zum rendern ben�tigt!)
    return renderer;
}

bool Core::isInstanced()
{
    return instance!=NULL;
}

Core* Core::getInstance()
{
    //singleton
    if(instance==NULL)
        return (instance =(new Core()));
    return instance;
}

Core* Core::getInstance(int width, int height, string windowTitle)
{
    //singleton
    if(instance==NULL)
        return (instance = (new Core(width, height, windowTitle)));
    return instance;
}

int Core::init()
{
    //initialisiert sdl, sdl_image, sdl_ttf, sdl_mixer
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
        return -1;

    if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
        return -2;

    //erstellt ein sdl fenster mit titel weite und h�he
    window = SDL_CreateWindow(WINDOW_TITLE.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

    if(window == NULL)
        return -3;

    SDL_Surface* iconSurface = IMG_Load("icon.ico");
    SDL_SetWindowIcon(window, iconSurface);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if(renderer == NULL)
        return -4;


    //setzt den standard hintergrund
    SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );


    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) )
        return -5;

    if(TTF_Init() == -1)
        return -6;

    if(Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0)
    {
        return -7;
    }
    int size = 50;
    if(Core::WINDOW_HEIGHT == 480)
        size=40;
    else if(Core::WINDOW_HEIGHT == 600)
        size=48;
    else if(Core::WINDOW_HEIGHT == 768)
        size = 58;

    largeFont = TTF_OpenFont("FUTURAB.ttf", size);
    mediumFont = TTF_OpenFont("FUTURAB.ttf", size/2);
    smallFont = TTF_OpenFont("FUTURAB.ttf", size/4);

    if(largeFont == NULL || mediumFont == NULL || smallFont == NULL)
    {
        cout<<"font fail"<<endl;
        return -8;
    }
    return 0;
}

void Core::clean()
{
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    TTF_CloseFont(largeFont);
    TTF_CloseFont(mediumFont);
    TTF_CloseFont(smallFont);
    window = NULL;
    renderer = NULL;
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();

    delete instance;
}
