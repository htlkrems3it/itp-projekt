/// @Author B�hmi

#include "Animation.h"
#include <iostream>

using namespace std;

//descr:        Animation Description Identifier (ADI)
//frameDelta:   Frames, bis auf den n�chsten Frame gesprungen wird
Animation::Animation(string descr, int frameDelta)
{
    Animation::textureCount = 0;
    Animation::frameCount = 0;
    Animation::descr = descr;
    Animation::frameDelta = frameDelta;
    Animation::textures = new list<Texture*>();
    Animation::i = 0;
}

// Setzt frameCount auf 0
void Animation::reset()
{
    Animation::frameCount = 0;
}

// Gibt die aktuelle Textur zur�ck
Texture* Animation::getTexture()
{
    list<Texture*>::iterator i = textures->begin();
    advance(i, frameCount);
    incFrameCount();
    return *i;
}

void Animation::addTexture(Texture* texture)
{
    textureCount++;
    textures->push_back(texture);
}

// Setzt frameCount auf frameCount + 1
// Wenn frameCount die Anzahl der Texturen �bersteigt, wird es auf 0 gesetzt
void Animation::incFrameCount()
{
    if(frameDelta == -1) return;

    if(i < frameDelta -1)
        i++;
    else
    {
        i = 0;

        if(Animation::frameCount + 1 >= Animation::textureCount)
            frameCount = 0;
        else
            Animation::frameCount++;
    }
}

void Animation::addImage(string path, SDL_Rect* size)
{
    Texture* t = new Texture();
    if(0!=t->loadImage(path, NULL, size))
        cout<<"Failed to load Texture!"<<endl;
   // delete size;
    Animation::addTexture(t);
}

void Animation::addText(string text, SDL_Color* color, short size)
{
    Texture* t = new Texture();
    if(0 !=t->loadText(text, color, size))
    {
        cout<<"Failed to load text: "<<text<<endl;
    }
    delete color;
    Animation::addTexture(t);
}

// Setzt frameDelta
void Animation::setDelta(int frameDelta)
{
    Animation::frameDelta = frameDelta;
}

// Destruktor
Animation::~Animation()
{
    delete textures;
}

void Animation::clear()
{
    for(list<Texture*>::iterator i = textures->begin(); i != textures->end(); i++)
    {
        (*i)->clear();
        delete (*i);
    }
}
