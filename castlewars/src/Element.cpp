///@Author B�hmi

#include "Element.h"
#include <iostream>
using namespace std;

int defloop(Element* e) {return 0; }
int defevent(Element* e, SDL_Event* ev) {return 0; }


// loc:             Die Location, auf der das Element erscheint
// animations:      Ein Pointer auf den Start einer Liste von Animations-Pointern
// animationCount:  Anzahl der Animationen
Element::Element(Location* loc, string descr, bool isCollidable)
{
    Element::isCollidable = isCollidable;
    Element::frame = 0;
    Element::loop = &defloop;
    Element::event = &defevent;
    Element::descr = descr;
    Element::location = loc;
    Element::animations = new list<Animation*>();
    flipped = false;
}

// Gibt die Location zur�ck
Location* Element::getLocation()
{
    return Element::location;
}

// Gibt die ID zur�ck
int Element::getId()
{
    return Element::id;
}

// Setzt die CurrentAnimation auf einen index im animations-Array
void Element::setAnimation(int index)
{
    list<Animation*>::iterator it = animations->begin();
    advance(it, index);
    currentAnimation = *it;
}

void Element::addAnimation(Animation* animation)
{
    if(!animation) return;
    animations->push_back(animation);
    collisionRect = animation->getTexture()->size;

    if(currentAnimation == NULL)
        setAnimation(0);
}

// Ruft die Render-Methode der aktuellen Textur der aktuellen Animation auf
int Element::render()
{
    frame++;
    return (currentAnimation->getTexture())->render(location, flipped);
}

// Destruktor
Element::~Element()
{
    cout << "Exiting Element" << endl;
    delete collisionRect;
    delete location;
    clear();
    delete animations;
}

void Element::clear()
{
    for(list<Animation*>::iterator i = animations->begin(); i != animations->end(); i++)
    {
        (*i)->clear();
        animations->remove(*i);
        delete (*i);
    }
}
