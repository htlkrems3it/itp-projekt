///@author Kaupper & B�hmi

#include "Scene.h"
#include <iostream>

using namespace std;

int defloop(Scene* s){return 0;}
int defevent(Scene* s, SDL_Event* e){return 0;}

int Scene::removeElement(string name)
{
    for(list<Element*>::iterator i = elements->begin(); i != elements->end(); i++)
    {
        if((*i)->descr == name)
        {
            (*i)->clear();
            elements->remove(*i);
            return 0;
        }
    }

    return 1;
}

Scene::Scene(int id, string name, Game* game)
{
    Scene::loop = &defloop;
    Scene::event = &defevent;
    //zuweisungen
    Scene::game     = game;  //game die die scene beinhaltet
    Scene::id       = id;    //id der scene
    Scene::name     = name;  //name der scene
    Scene::elements = new list<Element*>();//grafische elemente
}
int Scene::render()
{
    for(list<Element*>::iterator i = elements->begin(); i != elements->end(); i++)
    {
        //rendert alle elemente in der scene
        ((Element*)*(i))->render();
    }
    return 0;
}

Entity* Scene::getElement(string name)
{
    for(list<Element*>::iterator i = elements->begin(); i != elements->end(); i++)
    {
        if((*i)->descr == name)
            return ((Entity*)*(i));
    }
    return NULL;
}
int Scene::countElements()
{
    int counter = 0;
    for(list<Element*>::iterator i = elements->begin(); i != elements->end(); i++, counter++);
    return counter;

}

Entity* Scene::getElement(int id)
{   int counter = 0;
    for(list<Element*>::iterator i = elements->begin(); i != elements->end(); i++, counter++)
    {
        if(counter == id)
            return ((Entity*)*(i));
    }
    return NULL;
}

int Scene::addElement(Element* element)
{
    //f�gt ein element hinzu
    elements->push_back(element);
    return 0;
}

void Scene::removeElementAt(int index)
{
    //l�scht ein element
    list<Element*>::iterator it = elements->begin();
    advance(it, index);
    Scene::elements->erase(it);
    (*it)->clear();
}

void Scene::clear()
{
    /*
    for(int i = 0; i < elements->size(); i++)
    {
        removeElementAt(i);
    }
    */
    for(list<Element*>::iterator i = elements->begin(); i != elements->end(); i++)
    {
        (*(*i)).clear();
        delete (*i);
    }
}

Scene::~Scene()
{
    cout << "Exiting Scene" << endl;
}
