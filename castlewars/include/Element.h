///@Author B�hmi

#ifndef ELEMENT_H
#define ELEMENT_H

#include <SDL.h>
#include <string>
#include <list>
#include "Location.h"
#include "Animation.h"

class Element;

typedef int (*onElementLoop)(Element* element);
typedef int (*onElementEvent)(Element* element, SDL_Event* e);

using namespace std;

class Element
{
    public:
        Element(Location* loc, string descr = "", bool isCollidable = false);
        ~Element();
        Location* getLocation();
        int getId();
        void setAnimation(int index);
        void addAnimation(Animation* animation);
        virtual int render();
        onElementLoop loop;
        onElementEvent event;
        SDL_Rect* collisionRect = NULL;
        Location* location = NULL;
        bool flipped;
        void clear();
        int id;
        string descr;
        int frame;
        bool isCollidable = false;
        Animation* currentAnimation = NULL;
        list<Animation*>* animations = NULL;
    private:

};

#endif // ELEMENT_H
