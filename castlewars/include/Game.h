///@author Kaupper

#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "Core.h"
#include "Scene.h"
#include "Timer.h"

class Scene;

class Game{
private:
    Core* core;
    Scene* activeScene;
    Timer* timer;
    int loadMedia();
    int onLoop();
    int onEvent(SDL_Event*);
    int onRender();


public:
    bool running;
    void clear();
    int setActiveScene(string name);
    int setActiveScene(int index);
    list<Scene*>* scenes;
    int addScene(Scene* scene);
    Game(int width, int height, string title);
    virtual ~Game();

    int run();
};

#endif // GAME_H_INCLUDED
