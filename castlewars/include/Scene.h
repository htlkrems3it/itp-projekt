///@author Kaupper & B�hmi

#ifndef SCENE_H_INCLUDED
#define SCENE_H_INCLUDED

#include <SDL.h>
#include <list>
#include <string>
#include "Element.h"
#include "KeyboardLayout.h"
#include "Game.h"
#include "Entity.h"

using namespace std;

class Scene;
class Game;
class Entity;

typedef int (*onSceneLoop)(Scene* scene);
typedef int (*onSceneEvent)(Scene* scene, SDL_Event* event);

class Scene
{
public:
    ~Scene();
    Scene(int id, string name, Game* game);
    int render();
    int addElement(Element* element);
    void removeElementAt(int index);
    int removeElement(string name);
    Entity* getElement(string name);
    Entity* getElement(int it);
    int countElements();
    onSceneLoop loop;
    onSceneEvent event;
    string name;
    int id;

    void clear();
    list<Element*>* elements;
    Game* game;

private:
    // R.I.P Private Variablen
};

#endif // SCENE_H_INCLUDED
