#ifndef KEYBOARDLAYOUT_H
#define KEYBOARDLAYOUT_H

struct KeyboardLayout
{
    SDL_Keycode up;
    SDL_Keycode left;
    SDL_Keycode right;
    SDL_Keycode action;
};

#endif // KEYBOARDLAYOUT_H
