///@Author B�hmi

#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>
#include <list>
#include "Texture.h"

using namespace std;

class Animation
{
    public:
        int i;
        Animation(string descr, int frameDelta);
        virtual ~Animation();
        void reset();
        void addTexture(Texture* texture);
        Texture* getTexture();
        void incFrameCount();
        void setDelta(int frameDelta);
        void addImage(string path, SDL_Rect* rect);
        void addText(string text, SDL_Color* color, short size);
        void clear();
    private:
        list<Texture*>* textures = NULL;
        int textureCount;
        int frameDelta;
        int frameCount;
        string descr;
};

#endif // ANIMATION_H
