///@author Kaupper

#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED

#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

using namespace std;

class Core{
private:
    static Core* instance;

    string WINDOW_TITLE;

    SDL_Renderer* renderer;

    Core();
    Core(int width, int height, string windowTitle);

    int init();

public:
    SDL_Window* window;
    int WINDOW_HEIGHT;
    int WINDOW_WIDTH;
    TTF_Font* largeFont = NULL;
    TTF_Font* mediumFont = NULL;
    TTF_Font* smallFont = NULL;
    static bool isInstanced();
    static Core* getInstance();
    static Core* getInstance(int width, int height, string windowTitle);

    SDL_Renderer* getRenderer();

    void clean();
};

#endif // CORE_H_INCLUDED
