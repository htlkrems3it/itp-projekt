///@author Kaupper

#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include "Location.h"
#include "Core.h"

using namespace std;

class Texture{
public:
    Texture();
    virtual ~Texture();

    void clear();
    int loadText(string text, SDL_Color* color, short size = 0);
    int loadImage(string path, SDL_Rect* clip = NULL, SDL_Rect* size = NULL);
    int getWidth();
    int getHeight();
    int render(Location* loc, bool flip = false);
    bool flip;
    SDL_Rect* size = NULL;
private:
    SDL_Texture* texture = NULL;
    SDL_Rect* clip = NULL;
};

#endif // TEXTURE_H_INCLUDED
